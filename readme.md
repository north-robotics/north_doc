# North Robotics Python Libraries

Welcome to the documentation for the North Robotics Python libraries! This documentation covers libraries needed to 
operate the North C9 controller and North N9 robot, along with classes and utilities that can be used to construct 
workflows using robots, motors and other devices.

## Getting Started

### Requirements.txt

A basic [`requirements.txt`](https://gitlab.com/north-robotics/north_doc/blob/master/north_doc/examples/requirements.txt) 
file is available that you can use in your project. This installs the `north_utils`, `north_c9`, and `north_robots` 
libraries.

### North C9 Controller (`north_c9`)

Install the `north_c9` library
```bash
pip install git+https://gitlab.com/north-robotics/north_c9
```

Import the `C9Controller` class at the top of a Python file, or inside a Python console
```python
from north_c9.controller import C9Controller
```
   
Create a new `C9Controller` instance to connect to the C9 controller
```python     
controller = C9Controller()
```

Now you can move axes, toggle outputs and move the N9 (if available)
```python
# move axis 5 to 0 counts
controller.move_axis(5, 0, velocity=1000, acceleration=5000)
# rotate axis 5 once (motors have 1000 counts / revolution)
controller.move_axis(5, 1000, velocity=1000, acceleration=5000, relative=True)
# start spin axis 5
controller.spin_axis(5, velocity=1000, acceleration=5000)
# stop spinning axis 5
controller.spin_axis_stop(5)

# turn output 0 on and off
controller.output(0, True)
controller.output(0, False)
controller.output_toggle(0)

# home the N9
controller.home()
# move the N9 to x=0 mm, y=150 mm, z=150 mm and gripper=90 deg
controller.move_arm(0, 150, 150, gripper=90)
```

### North N9 (`north_robots`)

Install the `north_robots` library
```bash
pip install git+https://gitlab.com/north-robotics/north_robots
```

Import the `C9Controller` and `N9Robot` classes
```python
from north_c9.controller import C9Controller
from north_robots.n9 import N9Robot
```

Now create instances of the controller and robot
```python
controller = C9Controller()
robot = N9Robot(controller)
```

Now you can home and move the N9 robot arm around
```python
robot.home()
robot.move(0, 150)
robot.move(0, 150, 200)
robot.move(gripper=90)
robot.elbow.move_degrees(-90)
```


### North Components (`north_robots`)

The `north_robots` library also provides higher-level classes for locations and components. A getting started example
 can be found in the [examples/north_robots/getting_started.py](https://gitlab.com/north-robotics/north_doc/blob/master/north_doc/examples/north_robots/getting_started.py) file.


## Notes

### N9 Controller Layout

![N9 Controller Layout](https://gitlab.com/north-robotics/north_doc/raw/master/assets/n9_controller_layout.png)


### Nanotec Plug & Drive Configuration

Plug & Drive Studio 2.0 is required to configure the Nanotec CL4 motor controllers used by the C9. A minor 
configuration change in Plug & Drive is needed in order to work with the custom version of the firmware on the CL4s.

1. Navigate to `C:\Plug & Drive Studio\PNDS-2.0.5-x86` (your Plug & Drive installation folder may be different)
1. Edit `pnd_studio.ini` and add the following line to the bottom: `-Dpnds.controller.force.update.enable=false`