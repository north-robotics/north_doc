import time
from north_c9.controller import C9Controller
from north_c9.axis import RevoluteAxis, PrismaticAxis, Output
from north_robots.components import Component, Location, LocationPlaceholder
from north_robots.n9 import N9Robot


class BasicComponent(Component):
    """
    This is a basic example component that demonstrates using Axis classes and placeholder locations
    """

    # this location placeholder will be replaced with an instance attribute with the actual location when an object
    # is created. This location is looked up from a components.json file, otherwise the user is asked to drive the robot
    # to the location to capture and locate it.
    dropoff_location = LocationPlaceholder()

    # name is a required parameter, and location is optional, classes inheriting from component should specifiy both
    # other configuration variables can be added as keyword arguments
    # in their constructor to enable better autocompletion, a kwargs catchall should also be added for other component options
    def __init__(self, robot: N9Robot, name, location=None, linear_axis_number=5, spinner_axis_number=6, output_number=0,
                 linear_velocity_counts=1000, linear_acceleration_counts=5000, spin_velocity_rpm=1000, spin_acceleration_rpm=2000,
                 measurement_position_counts=7000, dropoff_safe_height_mm=100, **kwargs):
        # always call the Component constructor
        Component.__init__(self, name, location, **kwargs)  # super().__init__(name, location, **kwargs) works too

        self.linear_velocity_counts = linear_velocity_counts
        self.linear_acceleration_counts = linear_acceleration_counts
        self.spin_velocity_rpm = spin_velocity_rpm
        self.spin_acceleration_rpm = spin_acceleration_rpm

        self.measurement_position_counts = measurement_position_counts
        self.dropoff_safe_height_mm = dropoff_safe_height_mm

        # this is usually an N9Robot instance
        self.robot = robot
        self.linear_axis = PrismaticAxis(controller, linear_axis_number, counts_per_mm=10)
        self.spinner_axis = RevoluteAxis(controller, spinner_axis_number)
        self.output = Output(controller, output_number)

    # it's good practice to have a home method for components with axes
    def home(self):
        self.linear_axis.home()
        self.spinner_axis.home()

    def take_measurement(self):
        """ takes a fake "measurement" by moving the axes to a set position and turning on the output """

        # construct a safe location using the dropoff safe height
        safe_z = self.dropoff_location.z + self.dropoff_safe_height_mm
        dropoff_safe_location = self.dropoff_location.copy(z=safe_z)

        # drop off the sample
        self.robot.move_to_locations([dropoff_safe_location, self.dropoff_location])
        self.robot.gripper_output.open()
        self.robot.move_to_location(dropoff_safe_location)

        # move sample to measurement location
        self.linear_axis.move(self.measurement_position_counts, velocity=self.linear_velocity_counts, acceleration=self.linear_acceleration_counts)
        self.spinner_axis.spin(self.spin_velocity_rpm, self.spin_acceleration_rpm)

        # take "measurement"
        self.output.on()
        time.sleep(1)
        self.output.off()

        # move sample to pickup location
        self.spinner_axis.spin_stop()
        self.linear_axis.move(0, velocity=self.linear_velocity_counts, acceleration=self.linear_acceleration_counts)

        # pickup sample
        self.robot.move_to_location(self.dropoff_location)
        self.robot.gripper_output.close()
        self.robot.move_to_location(dropoff_safe_location)


# start by creating a connection to the C9
controller = C9Controller()

# create an instance of the N9 robot and our component
robot = N9Robot(controller)
component = BasicComponent(robot, 'basic_component')

# call Component.locate_all to prompt the user to drive to new location placeholders
Component.locate_all(controller)

# home the N9 and start using your component
robot.home()
component.take_measurement()
